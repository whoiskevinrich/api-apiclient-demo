using System.Web;
using System.Web.Http;
using DemoApi.Web;
using Swashbuckle.Application;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace DemoApi.Web
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1","DemoApi");
                    c.IncludeXmlComments($"{HttpRuntime.BinDirectory}/DemoApi.XML");
                    c.IncludeXmlComments($"{HttpRuntime.BinDirectory}/DemoApiLibrary.XML");
                    c.DescribeAllEnumsAsStrings();
                })
                .EnableSwaggerUi(c =>
                {
                    c.DisableValidator();
                });
        }
    }
}
