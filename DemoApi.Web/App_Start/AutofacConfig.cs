﻿using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using DemoApi.Library;

namespace DemoApi.Web
{
    public static class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType(typeof(ValueService)).AsImplementedInterfaces();
            builder.RegisterApiControllers(typeof(Controllers.ValuesController).Assembly);

            var container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}