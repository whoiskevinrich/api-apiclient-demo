﻿using System.Linq;
using System.Net;
using System.Web.Http;
using DemoApi.Library;
using Swashbuckle.Swagger.Annotations;

namespace DemoApi.Web.Controllers
{
    [RoutePrefix("api/values/v1")]
    [SwaggerResponse(HttpStatusCode.NotFound, "That value cannot be found")]
    public class ValuesController : ApiController
    {
        private readonly IValueService _values;

        public ValuesController(IValueService values)
        {
            _values = values;
        }

        /// <summary>
        /// Get inforamtion about all values
        /// </summary>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetValues()
        {
            var results = _values.GetAll().ToList();
            if(!results.Any()) return NotFound();
            return Ok(results);
        }

        /// <summary>
        /// Get information about a specific value
        /// </summary>
        /// <remarks>
        /// Method returns bad request in cases where the id supplied is not a positive integer.
        /// </remarks>
        /// <param name="id">The ValueId to retrieve</param>
        [HttpGet]
        [Route("{id:int}")]
        [SwaggerOperation("find-value-by-id")]
        [SwaggerResponse(HttpStatusCode.OK, 
            Description = "Value found", 
            Type = typeof(Value))]
        [SwaggerResponse(HttpStatusCode.BadRequest)]
        [SwaggerResponse(HttpStatusCode.NotFound, 
            Description = "That value does not exist")]
        public IHttpActionResult FindValue(int id)
        {
            if (id < 1) return BadRequest("id must be positive integer");
            var result = _values.Find(id);
            if (result == null) return NotFound();
            return Ok(result);
        }
    }
}
