﻿using System.Web.Mvc;

namespace DemoApi.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("/swagger");
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
