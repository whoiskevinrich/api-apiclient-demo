using System.Collections.Generic;
using System.Linq;

namespace DemoApi.Library
{
    public class ValueService : IValueService
    {
        private readonly IEnumerable<Value> _values = new List<Value>
        {
            new Value
            {
                ValueId = 1,
                Name = "Value 1",
                Description = "The first value"
            },
            new Value
            {
                ValueId = 2,
                Name = "Value 2",
                Description = "The second value"
            },
            new Value
            {
                ValueId = 3,
                Name = "Value 3",
                Description = "The third value"
            }
        };

        public IEnumerable<Value> GetAll()
        {
            return _values;
        }

        public Value Find(int id)
        {
            return _values.SingleOrDefault(x => x.ValueId == id);
        }

        public void Add(Value value) { }

        public void Edit(Value value) { }

        public void Delete(Value value) { }
    }
}