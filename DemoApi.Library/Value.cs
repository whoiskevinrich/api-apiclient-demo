﻿using System.ComponentModel.DataAnnotations;

namespace DemoApi.Library
{
    /// <summary>
    /// A value
    /// </summary>
    public class Value
    {
        /// <summary>
        /// Database Key
        /// </summary>
        [Required]
        public int ValueId { get; set; }

        /// <summary>
        /// The name of the value
        /// </summary>
        [Required]
        public string Name { get; set; }


        /// <summary>
        /// A user-friendly description
        /// </summary>
        [Required]
        public string Description { get; set; }
    }
}
