﻿using System.Collections.Generic;

namespace DemoApi.Library
{
    public interface IValueService
    {
        void Add(Value value);
        void Delete(Value value);
        void Edit(Value value);
        Value Find(int id);
        IEnumerable<Value> GetAll();
    }
}